package com.epam.pattern.uservalidation.pages;

import static com.epam.constants.SeleniumProperties.LOGIN_PAGE_URI;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private final WebDriver driver;

    @FindBy(css = "input#userName")
    private WebElement usernameBox;

    @FindBy(css = "input#password")
    private WebElement passwordBox;

    @FindBy(css = "button#login")
    private WebElement loginButton;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openLoginPage() {
        driver.get(LOGIN_PAGE_URI);
    }

    public UserPage enterLoginCredentials(String username, String password) {
        usernameBox.sendKeys(username);
        passwordBox.sendKeys(password);
        loginButton.click();
        return new UserPage(driver);
    }
}
