package com.epam.pattern.uservalidation.tests;

import com.epam.driverfactory.Driver;
import com.epam.driverfactory.DriverFactory;
import com.epam.exceptions.BrowserNotFoundException;
import com.epam.pattern.uservalidation.pages.LoginPage;
import com.epam.pattern.uservalidation.pages.UserPage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class ContainerUser {
    private final Logger logger = LogManager.getLogger(ContainerUser.class);

    protected static WebDriver driver;
    protected static Response response;
    protected static LoginPage loginPage;
    protected static UserPage userPage;

    protected static String createdUsername;
    protected static String createdPassword;

    @Before("@userValidation")
    public void setUp() throws BrowserNotFoundException, IOException {
        Driver browserType = DriverFactory.createDriver();
        driver = browserType.getDriver();
        driver.manage().window().maximize();
        loginPage = new LoginPage(driver);
    }

    @After
    public void screenshotForFailedScenarios(Scenario scenario) {
        if(scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, "image/png", scenario.getName());
            logger.error("Scenario has been failed and screenshot has been taken");
        }
    }

    @After("@userValidation")
    public static void quitDriver() {
        driver.quit();
    }
}
