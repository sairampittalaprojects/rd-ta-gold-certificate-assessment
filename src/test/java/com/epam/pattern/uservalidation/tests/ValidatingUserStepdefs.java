package com.epam.pattern.uservalidation.tests;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class ValidatingUserStepdefs {
    private final ContainerUser container;

    public ValidatingUserStepdefs(ContainerUser container) {
        this.container = container;
    }

    @Given("user navigates to the login page")
    public void userNavigatesToTheLoginPage() {
        ContainerUser.loginPage.openLoginPage();
    }

    @When("user enters the credentials")
    public void userEntersTheCredentials() {
        ContainerUser.userPage = ContainerUser.loginPage.enterLoginCredentials(ContainerUser.createdUsername, ContainerUser.createdPassword);
    }

    @Then("verify that the correct username is displayed")
    public void verifyThatTheCorrectUsernameIsDisplayed() {
        Assert.assertTrue(ContainerUser.userPage.verifyUserName(ContainerUser.createdUsername));
    }
}
