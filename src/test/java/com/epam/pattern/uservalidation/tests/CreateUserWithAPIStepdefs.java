package com.epam.pattern.uservalidation.tests;

import static io.restassured.RestAssured.*;

import static com.epam.constants.APIURIs.CREATE_USER_URI;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.json.JSONObject;
import org.testng.Assert;

public class CreateUserWithAPIStepdefs {
    private final ContainerUser container;

    public CreateUserWithAPIStepdefs(ContainerUser container) {
        this.container = container;
    }


    @Given("user sets the base uri for creating user")
    public void userSetsTheBaseUriForCreatingUser() {
        baseURI = CREATE_USER_URI;
    }

    @When("user sends post request with required parameters")
    public void userSendsPostRequestWithRequiredParameters(DataTable dataTable) {
        String userName = dataTable.row(0).get(0);
        String password = dataTable.row(0).get(1);
        JSONObject body = new JSONObject();
        body.put("userName", userName);
        body.put("password", password);

        ContainerUser.response = given()
                .contentType("application/json")
                .body(body.toString())
                .when()
                .post();

        storeLoginCredentials(userName, password);
    }

    @Then("validate that the status code is {int}")
    public void validateThatTheStatusCodeIs(int expectedStatusCode) {
        Assert.assertEquals(ContainerUser.response.getStatusCode(), expectedStatusCode);
    }

    @And("validate the response body")
    public void validateTheResponseBody(DataTable dataTable) {
        String expectedUsername = dataTable.row(0).get(0);
        Assert.assertEquals(ContainerUser.response.jsonPath().getString("username"), expectedUsername);
    }

    private void storeLoginCredentials(String userName, String password) {
        ContainerUser.createdUsername = userName;
        ContainerUser.createdPassword = password;
    }
}
