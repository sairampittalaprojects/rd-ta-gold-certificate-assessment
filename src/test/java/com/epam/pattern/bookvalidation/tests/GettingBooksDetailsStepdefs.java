package com.epam.pattern.bookvalidation.tests;

import static com.epam.constants.APIURIs.GET_BOOKS_DATA_URI;

import static io.restassured.RestAssured.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class GettingBooksDetailsStepdefs {
    private final ContainerBook container;

    public GettingBooksDetailsStepdefs(ContainerBook container) {
        this.container = container;
    }

    @Given("user sets the base uri for getting book details")
    public void userSetsTheBaseUriForGettingBookDetails() {
        baseURI = GET_BOOKS_DATA_URI;
    }

    @When("user sends the get request")
    public void userSendsTheGetRequest() {
        ContainerBook.response = given()
                .when()
                .get();
        ContainerBook.responseJsonPath = ContainerBook.response.jsonPath();
    }

    @Then("verify that the status code is {int}")
    public void verifyThatTheStatusCodeIs(int expectedStatusCode) {
        Assert.assertEquals(ContainerBook.response.getStatusCode(), expectedStatusCode);
    }
}
