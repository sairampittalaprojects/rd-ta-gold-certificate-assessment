package com.epam.pattern.bookvalidation.tests;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;

public class ValidatingBookDetailsStepdefs {
    private final ContainerBook container;

    public ValidatingBookDetailsStepdefs(ContainerBook container) {
        this.container = container;
    }

    @Given("user navigates to the book store homepage")
    public void userNavigatesToTheBookStoreHomepage() {
        ContainerBook.bookStoreHomePage.openBookStoreHomepage();
    }

    @Then("verify the title of Book Store is {string}")
    public void verifyTheTitleOfBookStoreIs(String title) {
        Assert.assertTrue(ContainerBook.bookStoreHomePage.verifyBookStoreTitle(title));
    }


    @Then("verify the title of Git Pocket Guide book")
    public void verifyTheTitleOfGitPocketGuideBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyGitPocketGuideTitle(ContainerBook.responseJsonPath.getString("books[0].title")));
    }
    @And("verify the author of Git Pocket Guide book")
    public void verifyTheAuthorOfGitPocketGuideBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyGitPocketGuideAuthor(ContainerBook.responseJsonPath.getString("books[0].author")));
    }
    @And("verify the publisher of Git Pocket Guide book")
    public void verifyThePublisherOfGitPocketGuideBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyGitPocketGuidePublisher(ContainerBook.responseJsonPath.getString("books[0].publisher")));

    }

    @Then("verify the title of Learning Javascript Design book")
    public void verifyTheTitleOfLearningJavascriptDesignBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyLearningJavascriptDesignTitle(ContainerBook.responseJsonPath.getString("books[1].title")));
    }
    @And("verify the author of Learning Javascript Design book")
    public void verifyTheAuthorOfLearningJavascriptDesignBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyLearningJavascriptDesignAuthor(ContainerBook.responseJsonPath.getString("books[1].author")));
    }
    @And("verify the publisher of Learning Javascript Design book")
    public void verifyThePublisherOfLearningJavascriptDesignBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyLearningJavascriptDesignPublisher(ContainerBook.responseJsonPath.getString("books[1].publisher")));
    }

    @Then("verify the title of Designing Evolvable Web API book")
    public void verifyTheTitleOfDesigningEvolvableWebAPIBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyDesigningEvolvableWebAPITitle(ContainerBook.responseJsonPath.getString("books[2].title")));
    }
    @And("verify the author of Designing Evolvable Web API book")
    public void verifyTheAuthorOfDesigningEvolvableWebAPIBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyDesigningEvolvableWebAPIAuthor(ContainerBook.responseJsonPath.getString("books[2].author")));
    }
    @And("verify the publisher of Designing Evolvable Web API book")
    public void verifyThePublisherOfDesigningEvolvableWebAPIBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyDesigningEvolvableWebAPIPublisher(ContainerBook.responseJsonPath.getString("books[2].publisher")));
    }

    @Then("verify the title of Speaking Javascript book")
    public void verifyTheTitleOfSpeakingJavascriptBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifySpeakingJavascriptTitle(ContainerBook.responseJsonPath.getString("books[3].title")));
    }
    @And("verify the author of Speaking Javascript book")
    public void verifyTheAuthorOfSpeakingJavascriptBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifySpeakingJavascriptAuthor(ContainerBook.responseJsonPath.getString("books[3].author")));
    }
    @And("verify the publisher of Speaking Javascript book")
    public void verifyThePublisherOfSpeakingJavascriptBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifySpeakingJavascriptPublisher(ContainerBook.responseJsonPath.getString("books[3].publisher")));
    }

    @Then("verify the title of You Don't Know JS book")
    public void verifyTheTitleOfYouDonTKnowJSBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyYouDonTKnowJSTitle(ContainerBook.responseJsonPath.getString("books[4].title")));
    }
    @And("verify the author of You Don't Know JS book")
    public void verifyTheAuthorOfYouDonTKnowJSBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyYouDonTKnowJSAuthor(ContainerBook.responseJsonPath.getString("books[4].author")));
    }
    @And("verify the publisher of You Don't Know JS book")
    public void verifyThePublisherOfYouDonTKnowJSBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyYouDonTKnowJSPublisher(ContainerBook.responseJsonPath.getString("books[4].publisher")));
    }

    @Then("verify the title of Programming Javascript Applications book")
    public void verifyTheTitleOfProgrammingJavascriptApplicationsBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyProgrammingJavascriptApplicationsTitle(ContainerBook.responseJsonPath.getString("books[5].title")));
    }
    @And("verify the author of Programming Javascript Applications book")
    public void verifyTheAuthorOfProgrammingJavascriptApplicationsBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyProgrammingJavascriptApplicationsAuthor(ContainerBook.responseJsonPath.getString("books[5].author")));
    }
    @And("verify the publisher of Programming Javascript Applications book")
    public void verifyThePublisherOfProgrammingJavascriptApplicationsBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyProgrammingJavascriptApplicationsPublisher(ContainerBook.responseJsonPath.getString("books[5].publisher")));
    }

    @Then("verify the title of Eloquent Javascript book")
    public void verifyTheTitleOfEloquentJavascriptBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyEloquentJavascriptTitle(ContainerBook.responseJsonPath.getString("books[6].title")));
    }
    @And("verify the author of Eloquent Javascript book")
    public void verifyTheAuthorOfEloquentJavascriptBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyEloquentJavascriptAuthor(ContainerBook.responseJsonPath.getString("books[6].author")));
    }
    @And("verify the publisher of Eloquent Javascript book")
    public void verifyThePublisherOfEloquentJavascriptBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyEloquentJavascriptPublisher(ContainerBook.responseJsonPath.getString("books[6].publisher")));
    }

    @Then("verify the title of Understanding ECMAScript book")
    public void verifyTheTitleOfUnderstandingECMAScriptBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyUnderstandingECMAScriptTitle(ContainerBook.responseJsonPath.getString("books[7].title")));
    }
    @And("verify the author of Understanding ECMAScript book")
    public void verifyTheAuthorOfUnderstandingECMAScriptBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyUnderstandingECMAScriptAuthor(ContainerBook.responseJsonPath.getString("books[7].author")));
    }
    @And("verify the publisher of Understanding ECMAScript book")
    public void verifyThePublisherOfUnderstandingECMAScriptBook() {
        Assert.assertTrue(ContainerBook.bookStoreHomePage
                .verifyUnderstandingECMAScriptPublisher(ContainerBook.responseJsonPath.getString("books[7].publisher")));
    }
}
