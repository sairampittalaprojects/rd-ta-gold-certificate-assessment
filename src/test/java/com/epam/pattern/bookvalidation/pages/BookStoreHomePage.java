package com.epam.pattern.bookvalidation.pages;

import static com.epam.constants.SeleniumProperties.BOOK_STORE_HOMEPAGE_URI;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BookStoreHomePage {
    private final WebDriver driver;

    @FindBy(css = "div[class='pattern-backgound playgound-header']>div")
    private WebElement bookStoreTitle;

    @FindBy(xpath = "(//div[@class='action-buttons'])[1]")
    private WebElement gitPocketGuideTitle;
    @FindBy(xpath = "(//div[@class='action-buttons'])[1]/following::div[1]")
    private WebElement gitPocketGuideAuthor;
    @FindBy(xpath = "(//div[@class='action-buttons'])[1]/following::div[2]")
    private WebElement gitPocketGuidePublisher;

    @FindBy(xpath = "(//div[@class='action-buttons'])[2]")
    private WebElement learningJavascriptDesignTitle;
    @FindBy(xpath = "(//div[@class='action-buttons'])[2]/following::div[1]")
    private WebElement learningJavascriptDesignAuthor;
    @FindBy(xpath = "(//div[@class='action-buttons'])[2]/following::div[2]")
    private WebElement learningJavascriptDesignPublisher;

    @FindBy(xpath = "(//div[@class='action-buttons'])[3]")
    private WebElement designingEvolvableWebAPITitle;
    @FindBy(xpath = "(//div[@class='action-buttons'])[3]/following::div[1]")
    private WebElement designingEvolvableWebAPIAuthor;
    @FindBy(xpath = "(//div[@class='action-buttons'])[3]/following::div[2]")
    private WebElement designingEvolvableWebAPIPublisher;

    @FindBy(xpath = "(//div[@class='action-buttons'])[4]")
    private WebElement titleOfSpeakingJavascriptTitle;
    @FindBy(xpath = "(//div[@class='action-buttons'])[4]/following::div[1]")
    private WebElement titleOfSpeakingJavascriptAuthor;
    @FindBy(xpath = "(//div[@class='action-buttons'])[4]/following::div[2]")
    private WebElement titleOfSpeakingJavascriptPublisher;

    @FindBy(xpath = "(//div[@class='action-buttons'])[5]")
    private WebElement titleOfYouDonTKnowJSTitle;
    @FindBy(xpath = "(//div[@class='action-buttons'])[5]/following::div[1]")
    private WebElement titleOfYouDonTKnowJSAuthor;
    @FindBy(xpath = "(//div[@class='action-buttons'])[5]/following::div[2]")
    private WebElement titleOfYouDonTKnowJSPublisher;

    @FindBy(xpath = "(//div[@class='action-buttons'])[6]")
    private WebElement programmingJavascriptApplicationsTitle;
    @FindBy(xpath = "(//div[@class='action-buttons'])[6]/following::div[1]")
    private WebElement programmingJavascriptApplicationsAuthor;
    @FindBy(xpath = "(//div[@class='action-buttons'])[6]/following::div[2]")
    private WebElement programmingJavascriptApplicationsPublisher;

    @FindBy(xpath = "(//div[@class='action-buttons'])[7]")
    private WebElement eloquentJavascriptTitle;
    @FindBy(xpath = "(//div[@class='action-buttons'])[7]/following::div[1]")
    private WebElement eloquentJavascriptAuthor;
    @FindBy(xpath = "(//div[@class='action-buttons'])[7]/following::div[2]")
    private WebElement eloquentJavascriptPublisher;

    @FindBy(xpath = "(//div[@class='action-buttons'])[8]")
    private WebElement understandingECMAScriptTitle;
    @FindBy(xpath = "(//div[@class='action-buttons'])[8]/following::div[1]")
    private WebElement understandingECMAScriptAuthor;
    @FindBy(xpath = "(//div[@class='action-buttons'])[8]/following::div[2]")
    private WebElement understandingECMAScriptPublisher;

    public BookStoreHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openBookStoreHomepage(){
        driver.get(BOOK_STORE_HOMEPAGE_URI);
    }
    public boolean verifyBookStoreTitle(String expectedTitle) {
        return bookStoreTitle.getText().equals(expectedTitle);
    }

    public boolean verifyGitPocketGuideTitle(String expected) {
        return gitPocketGuideTitle.getText().equals(expected);
    }
    public boolean verifyGitPocketGuideAuthor(String expected) {
        return gitPocketGuideAuthor.getText().equals(expected);
    }
    public boolean verifyGitPocketGuidePublisher(String expected) {
        return gitPocketGuidePublisher.getText().equals(expected);
    }

    public boolean verifyLearningJavascriptDesignTitle(String expected) {
        return learningJavascriptDesignTitle.getText().equals(expected);
    }
    public boolean verifyLearningJavascriptDesignAuthor(String expected) {
        return learningJavascriptDesignAuthor.getText().equals(expected);
    }
    public boolean verifyLearningJavascriptDesignPublisher(String expected) {
        return learningJavascriptDesignPublisher.getText().equals(expected);
    }

    public boolean verifyDesigningEvolvableWebAPITitle(String expected) {
        return designingEvolvableWebAPITitle.getText().equals(expected);
    }
    public boolean verifyDesigningEvolvableWebAPIAuthor(String expected) {
        return designingEvolvableWebAPIAuthor.getText().equals(expected);
    }
    public boolean verifyDesigningEvolvableWebAPIPublisher(String expected) {
        return designingEvolvableWebAPIPublisher.getText().equals(expected);
    }

    public boolean verifySpeakingJavascriptTitle(String expected) {
        return titleOfSpeakingJavascriptTitle.getText().equals(expected);
    }
    public boolean verifySpeakingJavascriptAuthor(String expected) {
        return titleOfSpeakingJavascriptAuthor.getText().equals(expected);
    }
    public boolean verifySpeakingJavascriptPublisher(String expected) {
        return titleOfSpeakingJavascriptPublisher.getText().equals(expected);
    }

    public boolean verifyYouDonTKnowJSTitle(String expected) {
        return titleOfYouDonTKnowJSTitle.getText().equals(expected);
    }
    public boolean verifyYouDonTKnowJSAuthor(String expected) {
        return titleOfYouDonTKnowJSAuthor.getText().equals(expected);
    }
    public boolean verifyYouDonTKnowJSPublisher(String expected) {
        return titleOfYouDonTKnowJSPublisher.getText().equals(expected);
    }

    public boolean verifyProgrammingJavascriptApplicationsTitle(String expected) {
        return programmingJavascriptApplicationsTitle.getText().equals(expected);
    }
    public boolean verifyProgrammingJavascriptApplicationsAuthor(String expected) {
        return programmingJavascriptApplicationsAuthor.getText().equals(expected);
    }
    public boolean verifyProgrammingJavascriptApplicationsPublisher(String expected) {
        return programmingJavascriptApplicationsPublisher.getText().equals(expected);
    }

    public boolean verifyEloquentJavascriptTitle(String expected) {
        return eloquentJavascriptTitle.getText().equals(expected);
    }
    public boolean verifyEloquentJavascriptAuthor(String expected) {
        return eloquentJavascriptAuthor.getText().equals(expected);
    }
    public boolean verifyEloquentJavascriptPublisher(String expected) {
        return eloquentJavascriptPublisher.getText().equals(expected);
    }

    public boolean verifyUnderstandingECMAScriptTitle(String expected) {
        return understandingECMAScriptTitle.getText().equals(expected);
    }
    public boolean verifyUnderstandingECMAScriptAuthor(String expected) {
        return understandingECMAScriptAuthor.getText().equals(expected);
    }
    public boolean verifyUnderstandingECMAScriptPublisher(String expected) {
        return understandingECMAScriptPublisher.getText().equals(expected);
    }
}
