package com.epam.runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
glue = {"com.epam"}, features = {"src/test/resources/features/bookValidation.feature"})
public class BookValidationRunnerTest extends AbstractTestNGCucumberTests {
}
