package com.epam.driverfactory;

import com.epam.exceptions.BrowserNotFoundException;
import com.epam.driverfactory.drivers.MyChromeDriver;
import com.epam.driverfactory.drivers.MyEdgeDriver;
import com.epam.driverfactory.drivers.MyFirefoxDriver;
import com.epam.utilities.FileReader;

import java.io.IOException;

import static com.epam.constants.SeleniumProperties.BROWSER_PROPERTIES_PATH;

public class DriverFactory {
    public static Driver createDriver() throws BrowserNotFoundException, IOException {
        return switch(FileReader.readPropertiesFile(BROWSER_PROPERTIES_PATH).getProperty("browser")) {
            case "chrome" -> new MyChromeDriver();
            case "edge" -> new MyEdgeDriver();
            case "firefox" -> new MyFirefoxDriver();
            default -> throw new BrowserNotFoundException("Please provide a valid browser");
        };
    }
}
