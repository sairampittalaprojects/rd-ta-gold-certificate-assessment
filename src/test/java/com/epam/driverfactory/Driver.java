package com.epam.driverfactory;

import org.openqa.selenium.WebDriver;

public interface Driver {
    public WebDriver getDriver();
}
