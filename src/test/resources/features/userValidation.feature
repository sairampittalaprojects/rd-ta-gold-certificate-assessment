Feature: Creating User and validating User
  Scenario Outline: Creating an User
    Given user sets the base uri for creating user
    When user sends post request with required parameters
    |<userName>|<password>|
    Then validate that the status code is 201
    And validate the response body
    |<userName>|

    Examples:
    |userName|password|
    |Sairam31|Sairam@150|

  @userValidation
  Scenario: Validating the user through Login
    Given user navigates to the login page
    When user enters the credentials
    Then verify that the correct username is displayed

