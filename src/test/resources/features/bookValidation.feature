Feature: Retrieving Books data and Validating
  Scenario: Capture the details of all books using API
    Given user sets the base uri for getting book details
    When user sends the get request
    Then verify that the status code is 200

  @bookValidation
  Scenario: Opening the Book Store Homepage
    Given user navigates to the book store homepage
    Then verify the title of Book Store is "Book Store"

  Scenario: verifying the details of Git Pocket Guide book
    Then verify the title of Git Pocket Guide book
    And verify the author of Git Pocket Guide book
    And verify the publisher of Git Pocket Guide book

  Scenario: verifying the details of Learning Javascript Design book
    Then verify the title of Learning Javascript Design book
    And verify the author of Learning Javascript Design book
    And verify the publisher of Learning Javascript Design book
#
  Scenario: verifying the details of Designing Evolvable Web API book
    Then verify the title of Designing Evolvable Web API book
    And verify the author of Designing Evolvable Web API book
    And verify the publisher of Designing Evolvable Web API book
#
  Scenario: verifying the details of Speaking Javascript book
    Then verify the title of Speaking Javascript book
    And verify the author of Speaking Javascript book
    And verify the publisher of Speaking Javascript book

  Scenario: verifying the details of You Don't Know JS book
    Then verify the title of You Don't Know JS book
    And verify the author of You Don't Know JS book
    And verify the publisher of You Don't Know JS book

  Scenario: verifying the details of Programming Javascript Applications book
    Then verify the title of Programming Javascript Applications book
    And verify the author of Programming Javascript Applications book
    And verify the publisher of Programming Javascript Applications book

  Scenario: verifying the details of Eloquent Javascript book
    Then verify the title of Eloquent Javascript book
    And verify the author of Eloquent Javascript book
    And verify the publisher of Eloquent Javascript book

  @bookValidationFinish
  Scenario: verifying the details of Understanding ECMAScript book
    Then verify the title of Understanding ECMAScript book
    And verify the author of Understanding ECMAScript book
    And verify the publisher of Understanding ECMAScript book
