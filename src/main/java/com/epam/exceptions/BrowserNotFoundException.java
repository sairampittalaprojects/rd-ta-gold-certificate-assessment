package com.epam.exceptions;

public class BrowserNotFoundException extends RuntimeException{
    public BrowserNotFoundException(String message) {
        super(message);
    }
}
