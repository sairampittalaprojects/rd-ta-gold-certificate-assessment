package com.epam.constants;

public class APIURIs {
    private APIURIs() {}

    public static final String CREATE_USER_URI = "https://demoqa.com/Account/v1/User";
    public static final String GET_BOOKS_DATA_URI = "https://demoqa.com/BookStore/v1/Books";
}
