package com.epam.constants;

public class SeleniumProperties {
    private SeleniumProperties() {}

    public static final String LOGIN_PAGE_URI = "https://demoqa.com/login";
    public static final String BOOK_STORE_HOMEPAGE_URI = "https://demoqa.com/books";
    public static final String BROWSER_PROPERTIES_PATH = "src/test/resources/browser.properties";
    public static final String CHROME_ARGUMENT = "--remote-allow-origins=*";
}
